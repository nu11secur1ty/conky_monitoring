# conky_monitoring

# Usage:
Before you run this program, you have to edit:
```
/home/nu11secur1ty/ >> (nu11secur1ty with your user), example:
/home/youruser/......
```
# Installation:

```
bash packages.sh
bash conky.sh
```

# OpenSUSE command for directly install:
![](https://github.com/nu11secur1ty/conky_monitoring/blob/master/cover/OpenSUSE_Logo.svg.png)
```
curl -L https://raw.githubusercontent.com/nu11secur1ty/conky_monitoring/master/suse.sh | bash
echo "bash /home/_your_user/.startconky.sh" >> /home/_your_user/.profile

```
# CentOS command for directly install:
![](https://github.com/nu11secur1ty/conky_monitoring/blob/master/cover/CentOS-7-logo-256x256.png)
```
curl -L https://raw.githubusercontent.com/nu11secur1ty/conky_monitoring/master/centos7.sh | bash
```

# Have fun with nu11secur1ty =).
